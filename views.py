# -*- coding: utf-8 -*-

from cloudmp_integration.views import UserBasedCloudMPTemplateView


class UserBased{{camel_case_app_name}}(UserBasedCloudMPTemplateView):
    """
    Hello World Example for UserBasedCloudMPTemplateView. This is the standard
    template used for Admin Pages.
    """
    context_attrs = UserBasedCloudMPTemplateView.context_attrs
    page_title = 'Page Title'
    tool_name = 'Tool Name'
    page_name = 'Page Name'
    template_name = '{{app_name}}/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['content'] = 'Hello World'
        return context
