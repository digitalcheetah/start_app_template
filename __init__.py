# -*- coding: utf-8 -*-

from nglogic_utils import get_version

__title__ = "__TITLE__"
__author__ = "AUTHOR <email@email.com>"
__license__ = "commercial"
__copyright__ = "Copyright 2016-2018 Digital Cheetah"

__version_info__ = {
    "major": 0,
    "minor": 0,
    "micro": 1,
    "releaselevel": "alpha",  # final | alpha | beta | rc
    "serial": 0,
    "dev": True,  # True | False
    "devserial": 0,  # dev release version number
}

__version__, __build__, __build_version__ = get_version(__version_info__)

default_app_config = "{{app_name}}.apps.Config"
