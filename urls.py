# -*- coding: utf-8 -*-

from django.urls import path

from {{app_name}} import views

app_name = "{{app_name}}"

urlpatterns = []
