# -*- coding: utf-8 -*-

from django.apps import AppConfig


class {{camel_case_app_name}}(AppConfig):
    """
    Configuration for {{camel_case_app_name}}.
    Ensure that you have added the following entry to the LOCAL_APPS list in
    settings.py:

        {{app_name}}.apps.{{camel_case_app_name}}
    """
    name = "{{app_name}}"
