# -*- coding: utf-8 -*-

from django.db import models

from cloudmp_integration.models import schema_version
from nglogic_utils.models import VMSSyncedAPIModel
from vmsapi.models import Organization


'''
class SampleSimpleModel(models.Model):
    """
    This is a sample simple model that has an 'organization' foreign key. Notice
    the values for 'on_delete' and 'db_constraint'. These are the desired values
    for Digital Cheetah database integrity.
    """

    organization = models.ForeignKey(
        Organization, null=False, on_delete=models.DO_NOTHING, db_constraint=False
    )


@schema_version(number=1)
class SampleSyncModel(VMSSyncedAPIModel):
    """
    This is a sample model that inherits Django fields that support syncing data
    from the MicroService to connected VMS sites. Notice the 'schema_version'
    class decorator, this needs to be changed every time that this model schema
    is updated.
    """

    organization = models.ForeignKey(
        Organization, null=False, on_delete=models.DO_NOTHING, db_constraint=False
    )
'''
